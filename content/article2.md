Title: Protect Biodiversity
Date: 31-08-2023 11:00
Category: Article
Tags: Medicine,News
Authors: Neena Bhandari
Summary: Protect biodiversity to secure traditional medicine sources

 
[SYDNEY] Traditional medicines and their natural sources must be protected from threats such as illegal wildlife trade to secure their role in narrowing the global health gap, say scientists.

For millions of marginalised communities, traditional medicine is the only recourse for meeting their primary healthcare needs, especially in remote and rural areas that lack access to formal healthcare systems.

The World Health Organization (WHO), which held its first-ever Traditional Medicine Global Summit in Gandhinagar, India, last week (17-18 August),  estimates that 80 per cent of people in most Asian and African countries use some form of traditional medicine for primary healthcare.